package server.events;


public class EventSomeTextFromSerial implements Event {

	private String msg;

	public EventSomeTextFromSerial(String mess) {
		this.msg = mess;
	}


	public String getMsg() {
		return this.msg;
	}

}
