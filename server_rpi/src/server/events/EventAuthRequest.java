package server.events;


public class EventAuthRequest implements Event {

	private final String username;
	private final String password;

	public EventAuthRequest(final String username, final String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
	
}
