package server.events;

public class EventTransmitToClient implements Event {
	private String message;

	public EventTransmitToClient(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
