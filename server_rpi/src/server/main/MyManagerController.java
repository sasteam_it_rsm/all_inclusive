package server.main;

import gnu.io.CommPortIdentifier;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;

import server.devices.Led;
import server.events.Event;
import server.events.EventAuthRequest;
import server.events.EventLInside;
import server.events.EventLOutside;
import server.events.EventSomeTextFromSerial;
import server.events.EventTransmissionCompleted;
import server.events.EventTransmitToClient;
import server.events.WaitForMessage;
import system.entities.BasicEventLoopController;
import client.requests.Msg;
import client.requests.MsgLVal;
import client.requests.MsgLog;
import client.requests.MsgStringTypes;
import client.requests.MsgTemp;

public class MyManagerController extends BasicEventLoopController {

	private final Led ledInside;
	private final Led ledFailed;
	private final SerialMonitor sm;
	private final BasicEventLoopController tcpServer;

	public MyManagerController(final Led ledInside, final Led ledFailde,
			final int portNumber) {
		this.ledInside = ledInside;
		this.ledFailed = ledFailde;
		this.sm = new SerialMonitor(this);

		System.out.println("Detecting Arduino...");
		int i = 0;
		String arduinoPort = "";
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum
					.nextElement();
			System.out.println("Detected Arduino on " + currPortId.getName());
			i++;
			arduinoPort = currPortId.getName();
		}

		if (i > 1) {
			throw new IllegalStateException(
					"Too many Arduino instances found, please connect only one Arduino!");
		}

		if (arduinoPort.isEmpty()) {
			throw new IllegalStateException("No Arduino instance found!");
		}

		System.out.println("Connecting to Arduino on " + arduinoPort);
		System.out.println("Starting Serial Monitor...");
		this.sm.start(arduinoPort, 9600);
		this.tcpServer = new TCPServer(portNumber, this);
		System.out.println("Starting the TCP Server...");
		this.tcpServer.start();
		this.tcpServer.notifyEvent(new WaitForMessage());
	}

	protected void processEvent(Event ev) {
		if (ev instanceof Msg) {
			// Messaggi da parte del client
			ev = (Msg) ev;
			if (ev instanceof MsgLVal) {
				System.out.println("Sending LVAL to Arduino...");
				this.sm.write('l');
			} else if (ev instanceof MsgTemp) {
				// Richiesta di consultare valore attuale di temperatura
				System.out.println("Sending TEMP to Arduino...");
				this.sm.write('t');
			} else if (ev instanceof MsgLog) {
				// Richiesta di consultare log accessi
				String responseToSend;
				try {
					responseToSend = MsgStringTypes.LOG.getVal() + "_"
							+ Utility.readLogFile().toString();
				} catch (IOException e) {
					System.out.println("Error: unable to access the log file!");
					return;
				}
				System.out.println("Sending LOG to Client...");
				this.tcpServer.notifyEvent(new EventTransmitToClient(
						responseToSend));
			} else
				throw new IllegalArgumentException();
		} else if (ev instanceof Event) {
			// Messaggi da arduino
			if (ev instanceof EventSomeTextFromSerial) {
				// Messaggio da ridirezionare al client.
				// Potrebbe essere una richiesta di LVal o di Temp, a
				// prescindere da ciò viene inviata al client
				EventSomeTextFromSerial event = (EventSomeTextFromSerial) ev;
				this.tcpServer.notifyEvent(new EventTransmitToClient(event
						.getMsg()));
			} else if (ev instanceof EventAuthRequest) {
				// Richiesta di autenticazione
				EventAuthRequest event = (EventAuthRequest) ev;

				String username = event.getUsername();
				String password = event.getPassword();
				boolean loginOk = false;

				try {
					loginOk = Utility.readLoginFile().contains(
							username + ":" + password);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				DateTimeFormatter dtf = DateTimeFormatter
						.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();

				System.out.println("Login attempt made by user: \"" + username
						+ "\" at " + dtf.format(now) + ". Status: "
						+ (loginOk ? "success!" : "rejected."));

				try {
					Utility.writeLineToLogFile("Login attempt made by user: \""
							+ username + "\" at " + dtf.format(now)
							+ ". Status: "
							+ (loginOk ? "success!" : "rejected."));
					int i = 0;
					while (i < 100) {
						this.sm.write(loginOk ? 'k' : 'x');
						Thread.sleep(1);
						i++;
					}
					

					if (!loginOk) {
						this.ledFailed.switchOn();
						Thread.sleep(100);
						this.ledFailed.switchOff();
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (ev instanceof EventTransmissionCompleted) {
				this.tcpServer.notifyEvent(new WaitForMessage());

			} else if (ev instanceof EventLInside) {
				try {
					this.ledInside.switchOn();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (ev instanceof EventLOutside) {
				try {
					this.ledInside.switchOff();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else
				throw new IllegalArgumentException();
		} else
			throw new IllegalArgumentException();
	}
}
