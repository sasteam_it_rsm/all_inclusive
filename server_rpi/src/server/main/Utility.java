package server.main;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class Utility {

	private static final Utility LOG = new Utility();
	private static final Path loginFile = Paths
			.get(javax.swing.filechooser.FileSystemView.getFileSystemView()
					.getHomeDirectory()
					+ File.separator
					+ "Desktop"
					+ File.separator + "logins.txt");

	private static final Path logFile = Paths
			.get(javax.swing.filechooser.FileSystemView.getFileSystemView()
					.getHomeDirectory()
					+ File.separator
					+ "Desktop"
					+ File.separator + "log.txt");
	private static final Charset utf8 = StandardCharsets.UTF_8;

	public static final int COMMON_PORT = 55555;

	private Utility() {
	}

	public static Utility getWriter() {
		return LOG;
	}

	/**
	 * Creates the logins file to be written. Default path: desktop If the file
	 * already exists, it will be deleted
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void createLoginFile() throws IOException {
		if (!loginFileExists()) {
			Files.write(loginFile, "".getBytes());
		} else {
			if (new File(loginFile.toString()).delete()) {
				Files.write(loginFile, "".getBytes());
			} else {
				throw new IOException();
			}
		}
	}

	/**
	 * Creates the log file to be written. Default path: desktop If the file
	 * already exists, it will be deleted
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void createLogFile() throws IOException {
		if (!logFileExists()) {
			Files.write(logFile, "".getBytes());
		} else {
			if (new File(logFile.toString()).delete()) {
				Files.write(logFile, "".getBytes());
			} else {
				throw new IOException();
			}
		}
	}

	/**
	 * Writes a single line into the login file (append).
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void writeLineToLoginFile(String arg) throws IOException {
		if (!loginFileExists()) {
			throw new IllegalStateException(
					"The file has not been created yet.");
		}

		List<String> lines = Arrays.asList(arg);
		Files.write(loginFile, lines, utf8, StandardOpenOption.CREATE,
				StandardOpenOption.APPEND);
	}

	/**
	 * Writes a single line into the log file (append).
	 * 
	 * @throws IOException
	 *             if an error occurs while creating the file
	 */
	public static void writeLineToLogFile(String arg) throws IOException {
		if (!logFileExists()) {
			throw new IllegalStateException(
					"The file has not been created yet.");
		}

		List<String> lines = Arrays.asList(arg);
		Files.write(logFile, lines, utf8, StandardOpenOption.CREATE,
				StandardOpenOption.APPEND);
	}

	/**
	 * Reads the login file;
	 * 
	 * @return
	 * @throws IOException
	 *             if an error occurs while reading the file
	 */
	public static List<String> readLoginFile() throws IOException {
		return FileUtils.readLines(loginFile.toFile(), "UTF-8");
	}

	/**
	 * Reads the login file;
	 * 
	 * @return
	 * @throws IOException
	 *             if an error occurs while reading the file
	 */
	public static List<String> readLogFile() throws IOException {
		return FileUtils.readLines(logFile.toFile(), "UTF-8");
	}

	private static boolean logFileExists() {
		return Files.exists(logFile);
	}

	private static boolean loginFileExists() {
		return Files.exists(loginFile);
	}

}
