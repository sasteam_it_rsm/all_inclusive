package server.main;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import client.requests.MsgStringTypes;

import server.events.ErrorEvent;
import server.events.Event;
import server.events.EventAuthRequest;
import server.events.EventLInside;
import server.events.EventLOutside;
import server.events.EventSomeTextFromSerial;
import system.entities.BasicEventLoopController;

/**
 * Simple Serial Monitor, adaptation from:
 * 
 * http://playground.arduino.cc/Interfacing/Java
 * 
 */
public class SerialMonitor implements SerialPortEventListener {
	SerialPort serialPort;

	/**
	 * A BufferedReader which will be fed by a InputStreamReader converting the
	 * bytes into characters making the displayed results codepage independent
	 */
	private BufferedReader input;
	/** The output stream to the port */
	private OutputStream output;
	/** Milliseconds to block while waiting for port open */
	private static final int TIME_OUT = 2000;

	private BasicEventLoopController observer;

	public SerialMonitor(BasicEventLoopController observer) {
		this.observer = observer;

	}

	public void start(String portName, int dataRate) {
		CommPortIdentifier portId = null;

		try {
			portId = CommPortIdentifier.getPortIdentifier(portName);
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(dataRate, SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(
					serialPort.getInputStream()));
			output = serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	/**
	 * This should be called when you stop using the port. This will prevent
	 * port locking on platforms like Linux.
	 */
	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	/**
	 * Handle an event on the serial port. Read the data and print it.
	 */
	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				String inputLine = input.readLine();
				System.out.println(inputLine);
				// MESSAGE SYNTAX FOR PARSING
				// authentication: auth_user_password\n
				// any other	 message: msg_messagetext\n
				
				

				Event ev = null;

				// If the string contains an authentication request, username
				// and password will be extracted
				if (inputLine.startsWith(MsgStringTypes.AUTH.getVal())) {
					ev = new EventAuthRequest(inputLine.split("_")[1],
							inputLine.split("_")[2]);
				} else if (inputLine.startsWith(MsgStringTypes.LVALUE.getVal())) {
					// Otherwise, the string is considered as a normal message
					ev = new EventSomeTextFromSerial(inputLine);
				} else if (inputLine.startsWith(MsgStringTypes.TEMP.getVal())) {
					ev = new EventSomeTextFromSerial(inputLine);
				} else if (inputLine.equals(MsgStringTypes.LINSIDE.getVal())) {
					ev = new EventLInside();
				} else if (inputLine.equals(MsgStringTypes.LOUTSIDE.getVal())) {
					ev = new EventLOutside();
				} else {
					// If none of the previous scenarios is valid, an error
					// event is notified
					ev = new ErrorEvent();
				}

				if (observer != null) {
					observer.notifyEvent(ev);
				}
				
			} catch (Exception e) {
			}
		}
	}

	public void write(char character) {
		try {
			output.write((int) character);
		} catch (IOException e) {
		}
	}

}