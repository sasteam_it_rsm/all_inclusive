package server.main;

import static client.requests.MsgStringTypes.LOG;
import static client.requests.MsgStringTypes.LVALUE;
import static client.requests.MsgStringTypes.TEMP;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import server.events.Event;
import server.events.EventTransmissionCompleted;
import server.events.EventTransmitToClient;
import server.events.WaitForMessage;
import system.entities.BasicEventLoopController;
import client.requests.MsgLVal;
import client.requests.MsgLog;
import client.requests.MsgTemp;

public class TCPServer extends BasicEventLoopController {

	private final int portNumber;
	private final BasicEventLoopController controller;
	private int lastPort;
	private String lastHost;
	private DataOutputStream outToClient;
	private ServerSocket welcomeSocket;

	public TCPServer(final int portNumber,
			final BasicEventLoopController controller) {
		this.portNumber = portNumber;
		this.controller = controller;
		try {
			this.welcomeSocket = new ServerSocket(this.portNumber);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.lastPort = -1;
		this.lastHost = "";
		this.outToClient = null;
	}

	@Override
	protected void processEvent(Event ev) {
		if (ev instanceof WaitForMessage) {
			try {
				String clientSentence;
				System.out.println("Server TCP ready and listening to port "
						+ Utility.COMMON_PORT + "...");

				Socket connectionSocket = welcomeSocket.accept();

				this.lastHost = connectionSocket.getInetAddress()
						.getHostAddress();
				this.lastPort = connectionSocket.getPort();

				BufferedReader inFromClient = new BufferedReader(
						new InputStreamReader(connectionSocket.getInputStream()));
				this.outToClient = new DataOutputStream(
						connectionSocket.getOutputStream());
				clientSentence = inFromClient.readLine();

				System.out.println(clientSentence.toUpperCase()
						+ " request message coming from " + this.lastHost
						+ " using port " + this.lastPort);

				if (clientSentence.equals(LOG.getVal())) {
					controller.notifyEvent(new MsgLog());
				} else if (clientSentence.equals(LVALUE.getVal())) {
					controller.notifyEvent(new MsgLVal());
				} else if (clientSentence.equals(TEMP.getVal())) {
					controller.notifyEvent(new MsgTemp());
				} else {
					outToClient.writeBytes("ERROR");
				}

			} catch (Exception e) {
				e.printStackTrace();

			}
		} else if (ev instanceof EventTransmitToClient) {
			if (this.lastPort == -1 || this.outToClient == null
					|| this.lastHost.isEmpty()) {
				throw new IllegalStateException();
			} else {
				String message = ((EventTransmitToClient) ev).getMessage();
				System.out.println("Sending the message to " + this.lastHost
						+ ":" + this.lastPort + "...");
				try {
					this.outToClient.writeBytes(message + '\n');
				} catch (IOException e) {
					System.out
							.println("An error has occurred during the data transmission to client!");
					return;
				}
				this.lastPort = -1;
				this.lastHost = "";
				this.outToClient = null;
				this.controller.notifyEvent(new EventTransmissionCompleted());
			}

		} else
			throw new IllegalArgumentException();
	}
}
