/*
 * Sviluppato da Carlo Noia, Jacopo Conti, Leonardo Papini.
 */
package server.main;

import java.io.IOException;

import server.devices.Led;

public class ServerMain {

	public static void main(String[] args) {

		Led LFailedAccess = new server.devices.Led(0); // 11
		Led LInside = new server.devices.Led(2); // 13

		try {
			System.out.println("Creating file with login information...");
			Utility.createLoginFile();
			Utility.writeLineToLoginFile("carlo:carlo");
			Utility.writeLineToLoginFile("popkornz:popkornz");
			Utility.writeLineToLoginFile("leospyke96:leospyke96");

			System.out.println("Creating file to write logs...");
			Utility.createLogFile();

			System.out.println("Testing led #1...");
			LFailedAccess.switchOn();
			Thread.sleep(100);
			LFailedAccess.switchOff();
			Thread.sleep(100);
			LFailedAccess.switchOn();
			Thread.sleep(100);
			LFailedAccess.switchOff();
			Thread.sleep(100);
			LFailedAccess.switchOn();
			Thread.sleep(100);
			LFailedAccess.switchOff();
			
			System.out.println("Testing led #2...");
			LInside.switchOn();
			Thread.sleep(100);
			LInside.switchOff();
			Thread.sleep(100);
			LInside.switchOn();
			Thread.sleep(100);
			LInside.switchOff();
			Thread.sleep(100);
			LInside.switchOn();
			Thread.sleep(100);
			LInside.switchOff();		
			

		} catch (IOException e) {
			System.out
					.println("Fatal error: can't write or read one or both the basic files!");
			return;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		MyManagerController manager = new MyManagerController(LInside,
				LFailedAccess, Utility.COMMON_PORT);

		System.out.println("Starting the manager...");
		manager.start();

	}
}
