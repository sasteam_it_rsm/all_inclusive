package client.events;

import server.events.Event;

public class SetConnectionParametersEvent implements Event {
	private String ip;
	private int port;

	public SetConnectionParametersEvent(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

}
