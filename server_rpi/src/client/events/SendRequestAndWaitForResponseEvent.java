package client.events;

import client.requests.MsgStringTypes;
import server.events.Event;

public class SendRequestAndWaitForResponseEvent implements Event {

	private String req;

	public SendRequestAndWaitForResponseEvent(MsgStringTypes msg) {
		this.req = msg.getVal();
	}

	public String getRequest() {
		return this.req;
	}

}
