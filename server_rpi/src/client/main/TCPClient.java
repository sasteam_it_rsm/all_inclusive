package client.main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Arrays;

import server.events.Event;
import system.entities.BasicEventLoopController;
import client.events.SendRequestAndWaitForResponseEvent;
import client.events.SetConnectionParametersEvent;
import client.requests.MsgStringTypes;

public class TCPClient extends BasicEventLoopController {

	private String ip;
	private int port;

	private Socket clientSocket;
	private BufferedReader inFromServer;

	private ClientMain gui;

	public TCPClient(ClientMain gui) {
		this.ip = "";
		this.port = -1;
		this.clientSocket = null;
		this.inFromServer = null;
		this.gui = gui;
	}

	@Override
	protected void processEvent(Event ev) {
		if (ev instanceof SendRequestAndWaitForResponseEvent) {
			if (port == -1 || ip.isEmpty()) {
				throw new IllegalStateException();
			}
			try {
				this.gui.toggleControls();

				String sentence = "";

				this.clientSocket = new Socket(this.ip, this.port);
				DataOutputStream outToServer = new DataOutputStream(
						clientSocket.getOutputStream());

				this.inFromServer = new BufferedReader(new InputStreamReader(
						clientSocket.getInputStream()));

				sentence = ((SendRequestAndWaitForResponseEvent) ev)
						.getRequest();

				System.out.println("Sending " + sentence.toUpperCase()
						+ " request from local port: "
						+ clientSocket.getLocalPort() + "...");

				outToServer.writeBytes(sentence + '\n');

				String response = inFromServer.readLine();
				clientSocket.close();

				if (response.startsWith(MsgStringTypes.LOG.getVal())) {
					response = response.replaceAll(MsgStringTypes.LOG.getVal()
							+ "_", "");
					response = response.substring(1);
					response = response.replace(']', ' ');
					this.gui.fillList(Arrays.asList(response.split(", ")));
				} else if (response.startsWith(MsgStringTypes.LVALUE.getVal())) {
					this.gui.showMessage(String.valueOf(((Integer.parseInt(response.replaceAll(
							MsgStringTypes.LVALUE.getVal(), "").replaceAll("_", "").replaceAll(" ", ""))*100)/255)) + "%");
				} else {
					this.gui.showMessage(response.replaceAll(
							MsgStringTypes.TEMP.getVal(), "").replaceAll("_", "").replaceAll(" ", "") + "°C");
				}

				this.gui.toggleControls();

			} catch (IOException ex) {
				ex.printStackTrace();
			}

		} else if (ev instanceof SetConnectionParametersEvent) {
			System.out.println("Setting new connection parameters...");
			this.ip = ((SetConnectionParametersEvent) ev).getIp();
			this.port = ((SetConnectionParametersEvent) ev).getPort();
		}

		else
			throw new IllegalArgumentException();

	}
}
