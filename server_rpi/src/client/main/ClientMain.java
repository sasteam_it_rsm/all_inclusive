package client.main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.events.SendRequestAndWaitForResponseEvent;
import client.events.SetConnectionParametersEvent;
import client.requests.MsgStringTypes;

import server.main.Utility;
import system.entities.BasicEventLoopController;

public class ClientMain extends JFrame {

	private static final long serialVersionUID = 7378636096320135090L;
	private final BasicEventLoopController tcpClient;
	private final DefaultListModel<String> listModel;
	private JTextField txtIP;
	private JTextField txtPort;
	private boolean activation = true;
	private JButton btnLog;
	private JButton btnTemp;
	private JButton btnLValue;

	public ClientMain() {
		System.out.println("Starting TCP Client...");
		this.tcpClient = new TCPClient(this);
		this.tcpClient.start();

		this.setSize(700, 400);
		this.setTitle("SmartDoor v1 TCP Client");

		JPanel mainPanel = new JPanel(new BorderLayout());
		this.setContentPane(mainPanel);

		JPanel topPanel = new JPanel(new BorderLayout());
		mainPanel.add(topPanel, BorderLayout.NORTH);

		this.btnLog = new JButton("Check Log");
		this.btnTemp = new JButton("Get Temperature");
		this.btnLValue = new JButton("Get LValue");

		this.listModel = new DefaultListModel<String>();

		JList<String> lstLog = new JList<String>(listModel);

		JPanel topTopPanel = new JPanel(new FlowLayout());
		topTopPanel.add(btnLog);
		topTopPanel.add(btnTemp);
		topTopPanel.add(btnLValue);
		topPanel.add(topTopPanel, BorderLayout.NORTH);

		JPanel southTopPanel = new JPanel(new FlowLayout());
		this.txtIP = new JTextField();
		this.txtIP.setText("127.0.0.1");
		txtIP.setColumns(15);
		JLabel lblPoints = new JLabel(":");
		this.txtPort = new JTextField();
		this.txtPort.setText(String.valueOf(Utility.COMMON_PORT));
		txtPort.setColumns(5);
		txtPort.setEditable(false);
		southTopPanel.add(txtIP);
		southTopPanel.add(lblPoints);
		southTopPanel.add(txtPort);
		topPanel.add(southTopPanel, BorderLayout.SOUTH);

		mainPanel.add(lstLog, BorderLayout.CENTER);

		btnLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tcpClient.notifyEvent(new SetConnectionParametersEvent(txtIP
						.getText(), Integer.parseInt(txtPort.getText())));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				tcpClient.notifyEvent(new SendRequestAndWaitForResponseEvent(
						MsgStringTypes.LOG));
			}
		});

		btnTemp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tcpClient.notifyEvent(new SetConnectionParametersEvent(txtIP
						.getText(), Integer.parseInt(txtPort.getText())));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				tcpClient.notifyEvent(new SendRequestAndWaitForResponseEvent(
						MsgStringTypes.TEMP));
			}
		});

		btnLValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tcpClient.notifyEvent(new SetConnectionParametersEvent(txtIP
						.getText(), Integer.parseInt(txtPort.getText())));
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				tcpClient.notifyEvent(new SendRequestAndWaitForResponseEvent(
						MsgStringTypes.LVALUE));

			}
		});

		this.setVisible(true);
	}

	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	public void fillList(List<String> lista) {
		this.listModel.clear();
		for (String s : lista) {
			this.listModel.addElement(s);
		}
	}

	public void toggleControls() {
		activation = !activation;
		this.btnLog.setEnabled(activation);
		this.btnLValue.setEnabled(activation);
		this.btnTemp.setEnabled(activation);
		this.txtIP.setEnabled(activation);
	}

	public static void main(String[] args) {
		new ClientMain();

	}
}
