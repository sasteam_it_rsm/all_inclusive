package client.requests;

public enum MsgStringTypes {

	LOG("log"), LVALUE("lvalue"), TEMP("temp"), AUTH("auth"), LINSIDE("linside"), LOUTSIDE(
			"loutside");

	private String value;

	MsgStringTypes(String value) {
		this.value = value;
	}

	public String getVal() {
		return this.value;
	}

}
