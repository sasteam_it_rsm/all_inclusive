package system.entities;

import server.events.Event;

public interface Observer {

	boolean notifyEvent(Event ev);
}
