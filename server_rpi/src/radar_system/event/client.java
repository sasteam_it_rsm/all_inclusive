package radar_system.event;

import java.io.*; 
import java.net.*; 

class Client {

	public static void main(String argv[]) throws Exception {

		String sentence = "";
		String modifiedSentence = "";
		
		
		
		while(!sentence.equals("quit")) {
				BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
				Socket clientSocket = new Socket("localhost", 6789);
        		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        		System.out.println("PORTA SUL CLIENT: " + clientSocket.getLocalPort());
        		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                	System.out.println("Insert sentence:");
                	sentence = inFromUser.readLine(); 
               		outToServer.writeBytes(sentence + '\n'); 
               		modifiedSentence = inFromServer.readLine(); 
               		System.out.println("FROM SERVER: " + modifiedSentence);
               		clientSocket.close();
        		
		}
		
	}
}