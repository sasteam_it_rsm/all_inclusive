package radar_system.event;

import java.io.*;
import java.net.*;

class Server {

	public static void main(String argv[]) throws Exception {

		String clientSentence; 
		String capitalizedSentence;

		ServerSocket welcomeSocket = new ServerSocket(6789); 
		//System.out.println(welcomeSocket.getLocalPort());
		while(true) {
			Socket connectionSocket = welcomeSocket.accept();
			System.out.println(connectionSocket.getInetAddress().getHostAddress());
			System.out.println("PORTA DEL CLIENT VISTA DAL SERVER: " + connectionSocket.getPort());
			System.out.println(connectionSocket.getLocalPort());
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			clientSentence = inFromClient.readLine();
			//System.out.println(clientSentence);
			capitalizedSentence = clientSentence.toUpperCase() + '\n'; 
			outToClient.writeBytes(capitalizedSentence);
		}
	}
}
