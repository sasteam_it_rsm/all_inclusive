#ifndef __PIR__
#define __PIR__

#include "MotionSensor.h"

class Pir: public MotionSensor {

public:
  Pir(int valuePin);
  bool isDetected();

private:
    int valuePin;
};

#endif
