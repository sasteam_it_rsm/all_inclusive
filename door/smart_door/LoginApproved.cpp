#include "LoginApproved.h"
#include "Pir.h"
#include "ButtonImpl.h"
#include "ServoTimer2.h"
#include "LedExt.h"
#include "MsgService.h"
#include "TempSensor.h"
#include "SoftwareSerial.h"

#define MAX_DELAY 200 //2 sec

int pirPin;
int btnPin;
int elapsedTimeTemp, elapsedTimeDetected;
int person;

Pir *pir;
ButtonImpl *button;
extern TempSensor* temp;
extern ServoTimer2 *servo;
extern LedExt *lValue;
extern MsgService msgService;
extern int state;
int alreadyDetected, messageIncoming;

LoginApproved::LoginApproved(int pir, int btn) {
  pirPin = pir;
  btnPin = btn;
}

void LoginApproved::init(int period) {
  Task::init(period);
  elapsedTimeTemp=0;
  elapsedTimeDetected=0;
  firstTime=1;
  person=0;
  pir = new Pir(pirPin);
  button = new ButtonImpl(btnPin);  
  alreadyDetected=0;
  messageIncoming=0;
}

void LoginApproved::tick() {   
  
  if(pir->isDetected()){
    alreadyDetected=1;
    person=1;
  }
  
  if(!alreadyDetected){
    if(!pir->isDetected() && elapsedTimeDetected >= MAX_DELAY){
      
      msgService.sendMsg(Msg("e"));
      servo->write(544); //0° chiusura porta
       lValue->setIntensity(0);
      person=0;
      elapsedTimeDetected=-1;
      state=0;
    }
  } 

  messageIncoming=(int)Serial.read();
  
  if(messageIncoming==108){ // l -> lvalue    
    Serial.println("lvalue_ " + (String)lValue->getIntensity());
  }else if(messageIncoming==116){ //t -> temperatura
    Serial.println("temp_"+ (String)temp->readTemperature());
  }

  if(person){
    
    
    servo->write(2100); //180° aperura porta
    if(firstTime){
      msgService.sendMsg(Msg("t_" + (String)temp->readTemperature()));  // t_20
      //.println((String)temp->readTemperature());
      //msgService.sendMsg(Msg("t_20"));  // t_20
      firstTime=0;
    }
    if(elapsedTimeTemp>=100){
      msgService.sendMsg(Msg("t_" + (String)temp->readTemperature()));  // t_20
      //msgService.sendMsg(Msg("t_20"));  // t_20
      elapsedTimeTemp=-1;
    }

    if( button->isPressed()){
       msgService.sendMsg(Msg("p"));
       lValue->setIntensity(0);
       servo->write(544);
       person=0;
       elapsedTimeDetected=-1;
       state=0;
       alreadyDetected=0;
    }
    
    if (msgService.isMsgAvailable()) {
        Msg* msg = msgService.receiveMsg();
         if(msg->getContent()=="KO"){
          lValue->setIntensity(0);
          servo->write(544);
          person=0;
          elapsedTimeDetected=-1;
          state=0;
          alreadyDetected=0;
        }else{  
          lValue->setIntensity(msg->getContent().toInt());
        }
        delete msg;
      }
  }else{
    elapsedTimeDetected++;
  }
  elapsedTimeTemp++;
  
}

