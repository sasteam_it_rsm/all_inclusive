#include "Pir.h"
#include "Arduino.h"
//#define CALIBRATION_TIME_SEC 30

Pir::Pir(int valuePin) {
  this->valuePin = valuePin;
  // Serial.println("Calibrating sensor on PIN " + valuePin);
  // for(int i = 0; i < CALIBRATION_TIME_SEC; i++){
  //   delay(1000);
  // }
  // Serial.println("PIR calibrated.");
}

bool Pir::isDetected() {
  int detected = digitalRead(this->valuePin);
  return detected == HIGH;
}
