#include "Login.h"
#include "Arduino.h"

String Login::splitString(String str, char sep, int index){
  int found = 0;
  int strIdx[] = { 0, -1 };
  int maxIdx = str.length() - 1; 
  String a = "ciao";
  String b;
  for (int i = 0; i <= maxIdx && found <= index; i++){
    if (str.charAt(i) == sep || i == maxIdx){
      found++;
      strIdx[0] = strIdx[1] + 1;
      strIdx[1] = (i == maxIdx) ? i+1 : i;
    }
  }
  return found > index ? str.substring(strIdx[0], strIdx[1]) : "";
}

void Login::setUser(String str){
  user=splitString(str, ':', 0);
}

void Login::setPassword(String str){
  password=splitString(str, ':', 1);
}

String Login::getUser(){  
  return user;
}

String Login::getPassword(){
  return password;
}
