/*
 * Sviluppato da Carlo Noia, Jacopo Conti, Leonardo Papini.
 */
#include "ServoTimer2.h"
#include <Arduino.h>
#include "Timer.h"
#include "config.h"
#include "LoginApproved.h"
#include "Idle.h"
#include "MsgService.h"
#include "SoftwareSerial.h"

int state;
Timer timer;
Idle idle(LON, LVALUE, SERVO, ECHO, TRIGGER, RXD, TXD);
LoginApproved loginApproved(PIR, BTNEXIT);
MsgService msgService(2,3);
void setup() {
  state = 0;
  timer.setupPeriod(10);  
  idle.init(10);
  loginApproved.init(10);  
  msgService.init(); 
  Serial.begin(9600);  
  while (!Serial){} 
}

void loop() {
  timer.waitForNextTick();
  if(state == 0) {
    if(idle.updateAndCheckTime(100)) {
      idle.tick();
    } 
  }else if(state==1){
    if(loginApproved.updateAndCheckTime(100)){
      loginApproved.tick();
    }
  }
}
