#ifndef __IDLE__
#define __IDLE__

#include "Task.h"


class Idle: public Task {  

  public:
    Idle(int lOn, int lValue, int servo, int echo, int trigger, int rxd, int txd);
    void init(int period);
    void tick();
};


#endif
