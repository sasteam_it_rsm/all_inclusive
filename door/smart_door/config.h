#ifndef __CONFIG__
#define __CONFIG__

#define LON 13
#define LVALUE 5
#define ECHO 7
#define TRIGGER 8
#define PIR 4
#define SERVO 10
#define BTNEXIT 9
#define TEMP4 4 
#define TEMP5 5
#define RXD 3
#define TXD 2

#endif
