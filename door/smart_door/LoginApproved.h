#ifndef __LOGINAPPROVED__
#define __LOGINAPPROVED__

#include "Task.h"


class LoginApproved: public Task {  

  public:
    LoginApproved(int pir, int btn);
    void init(int period);
    void tick();

  private:
    int firstTime;
};


#endif
