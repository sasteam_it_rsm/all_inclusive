#ifndef __MOTSENS__
#define __MOTSENS__

class MotionSensor {

public:
  virtual bool isDetected() = 0;
};

#endif
