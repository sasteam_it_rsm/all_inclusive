#include "Idle.h"
#include "Task.h"
#include "LedExt.h"
#include "Sonar.h"
#include "ServoTimer2.h"
#include <Arduino.h>
#include "MsgService.h"
#include "SoftwareSerial.h"
#include "Login.h"
#include "TempSensor.h"

#define MIN 544 /*0°*/
#define MAX 2100 /*180°*/
#define MIN_DIST 10 /*10 cm*/
#define MIN_SEC 200 /*2 sec*/

extern int state;
int servoPin;
int echoPin;
int triggerPin;
int lOnPin;
int lValuePin;
int rxdPin;
int txdPin;
int firstTime;
int elapsedTime;
int waitHandShake;
int waitLogin;
int tmp=0;
int readRaspberry=0;
char *p;

Led *lOn;
LedExt *lValue;
Sonar* prox;
ServoTimer2* servo;
Login *login;
Msg* msg;
TempSensor* temp;
extern MsgService msgService;

Idle::Idle(int lOn, int lValue, int servo, int echo, int trigger, int rxd, int txd) {
  lOnPin = lOn;
  lValuePin = lValue;
  servoPin = servo;
  echoPin = echo;
  triggerPin = trigger;
  rxdPin = rxd;
  txdPin = txd;
}

void Idle::init(int period) {
  firstTime=1;
  elapsedTime=0;
  waitHandShake=0;
  waitLogin=0; 
  Task::init(period);
  lOn = new Led(lOnPin);
  lValue = new LedExt(lValuePin);  
  prox = new Sonar(echoPin, triggerPin);
  servo = new ServoTimer2();
  temp = new TempSensor();
  servo->attach(servoPin);
  servo->write(544);
  lOn->switchOn();
  lValue->switchOn();
  lValue->setIntensity(0);  
}

void Idle::tick() {

  /*la prima volta che entro accendo LOn e imposto l'intensita a 0 di LVALUE*/
  if(firstTime){
     if(prox->getDistance()*100 <= MIN_DIST){
        elapsedTime++;
      }else if(prox->getDistance()*100 > MIN_DIST && elapsedTime < MIN_SEC){
        elapsedTime=0;
      }
      
      if(elapsedTime >= MIN_SEC){
        waitHandShake=1;    
        firstTime=0;
      }       
  }
  /*controllo se qualcuno permane davanti alla porta per un periodo e una distanza pre impostati*/
 

  if(waitHandShake){
    msgService.sendMsg(Msg("k"));
    if(msgService.isMsgAvailable()){
      Msg* msg = msgService.receiveMsg();
      if(msg->getContent() == "K"){
        waitHandShake=0;        
        waitLogin=1;
        elapsedTime=0;
      }
      delete msg;
    }
  }  

  if(waitLogin){
    int somethingArrived = 0;
    String myMsg;
    while(true) {
      if(msgService.isMsgAvailable()) {        
        msg = msgService.receiveMsg(); 
        //Serial.println("auth_" + login->splitString(msg->getContent(), ':', 0) +"_"+ login->splitString(msg->getContent(), ':', 1));
        myMsg += (String)msg->getContent();    
        somethingArrived = 1;       
      }
      else {
        if(somethingArrived) {
          break;  
        }        
      }
    }
    
    Serial.println("auth_" + myMsg);
    waitLogin=0; 
    tmp=1;  
    delete msg;       
  }  
  

  if (Serial.available() > 0) { 
      readRaspberry = (int)Serial.read();
  
      if(readRaspberry==108){ // l -> lvalue    
        Serial.println("lvalue_ " + (String)lValue->getIntensity());
      }else if(readRaspberry==116){ //t -> temperatura
        Serial.println("temp_"+ (String)temp->readTemperature());
      }
    
      if(readRaspberry==107){ // k -> tutto ok
        msgService.sendMsg(Msg("l")); //login avvenuto con sucesso
        elapsedTime=0;
        firstTime=1;
        waitLogin=0;
        state = 1;
      }else if(readRaspberry==120){ //x -> error
        msgService.sendMsg(Msg("x")); //login faild
        elapsedTime=0;
        firstTime=1;
        waitLogin=0;  
      }  
   }    
  
}


  
  
  
  
