#ifndef __LOGIN__
#define __LOGIN__

#include <Arduino.h>

class Login {  
public:
  Login();
  String splitString(String str, char sep, int index);
  String getUser(); 
  String getPassword();  
  void setUser(String str);
  void setPassword(String str);    

private:
  String user;
  String password;
};

#endif
