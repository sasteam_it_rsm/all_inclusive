﻿Repository del 4° progetto

RASPBERRY --> ARDUINO:
se tutto OK -> "k"
se rilavata presenza di errori -> "x"
richiesta di mostrare valore attuale LVal -> "l" (elle minuscola)
richiesta di mostrare valore attuale Temperatura -> "t"

ARDUINO --> RASPBERRY
passaggio del valore attuale di lvalue -> "lvalue_valoreAttuale"
passaggio del valore attuale di temp -> "temp_valoreAttuale"

ARDUINO --> ANDROID
per Handshake -> k (minuscola)
per login ok -> k (minuscola)
per login error -> x (minuscola)
per temperatura -> t_value

ANDROID --> ARDUINO
per Handshake -> k (maiuscola)