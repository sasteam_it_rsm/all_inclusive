package sasteam.smartdoorapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MonitoringActivity extends AppCompatActivity {

    private Button end;
    private SeekBar seekBar;
    private TextView temp;
    private TextView progressValue;
    public static Context mContext;
    private SmartDoor sd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring);
        mContext = getBaseContext();
        sd = new SmartDoor();
        end = (Button) findViewById(R.id.buttonEnd);
        seekBar = (SeekBar)findViewById(R.id.seekBarLed);
        temp = (TextView)findViewById(R.id.textViewTemp);
        progressValue = (TextView)findViewById(R.id.textViewProgress);

        temp.setText("0°");
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BTSingleton.getLog().btSocket!=null) //If the btSocket is busy
                {
                    try {
                        ConnectionManager.getInstance().getBtOutStream().write("KO".toString().getBytes());
                        Thread.sleep(500);
                        BTSingleton.getLog().btSocket.close(); //close connection
                        Intent i = new Intent(MonitoringActivity.this, PairActivity.class);
                        //Change the activity.
                        //i.putExtra(BTSingleton.EXTRA_ADDRESS, address);
                        startActivity(i);
                        System.exit(0);


                    } catch (Exception e) {

                    }
                }
            }
        });



        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser==true)
                {
                    try
                    {
                        int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                        progressValue.setText("" + (progress*100)/255 + "%");
                        progressValue.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2);
                        ConnectionManager.getInstance().getBtOutStream().write(String.valueOf(progress).toString().getBytes());
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // devo aggiornare la view ogni volta che mi arriva la temperatura
        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(2000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // update TextView here!
                                temp.setText("<");
                                temp.setText(ConnectionManager.getInstance().getTemp()+"°");
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();


    }
    public static Context getmContext() {
        return mContext;
    }
}
