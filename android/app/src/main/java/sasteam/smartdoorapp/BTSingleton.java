package sasteam.smartdoorapp;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;


import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Set;
import java.util.UUID;

public class BTSingleton {

    //Bluetooth
    private BluetoothAdapter myBluetooth = null;
    private String btAddress;
    BluetoothSocket btSocket = null;
    private Set<BluetoothDevice> pairedDevices;
    public static String EXTRA_ADDRESS = "device_address";
    private ArrayList<String> list;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private ConnectionManager cm;

    // inner static classes are initialised at first use
    private static class LazyHolder {
        private static final BTSingleton SINGLETON = new BTSingleton();
    }

    private BTSingleton() {
        //if the device has bluetooth
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        list = new ArrayList();
        btAddress = "";
    }

    // Creo il SINGLETON alla prima chiamata
    public static BTSingleton getLog() {
        return LazyHolder.SINGLETON;
    }

    public boolean init()
    {
        if (myBluetooth != null && myBluetooth.isEnabled()) {
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice bt : pairedDevices) {
                    list.add(bt.getName() + "_" + bt.getAddress()); //Get the device's name and the address
                    return true;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    public boolean connect() {
        try {
            BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(this.btAddress);//connects to the device's address and checks if it's available
            btSocket = dispositivo.createRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
            btSocket.connect();//start connection

            cm = ConnectionManager.getInstance();
            cm.setChannel(btSocket);
            cm.start();
        }
        catch (Exception ex) {
            Log.d("log --> " , ex.getMessage());
            return false;
        }
        return true;

    }

    public void setAddress(String address){
        this.btAddress = address;
    }

    /**
     * Restituisce l'elenco dei devices bluetooth trovati nelle vicinanze.
     * @return
     */
    public List<String> getDevices () {
        return Collections.unmodifiableList(this.list);
    }
}