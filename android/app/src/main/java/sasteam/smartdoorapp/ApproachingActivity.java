package sasteam.smartdoorapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import java.util.UUID;

public class ApproachingActivity extends AppCompatActivity {

    String address = null;
    private char vaffa = 'a';
    private ProgressDialog progress;
    public static Context mContext;

    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Intent newint = this.getIntent();
        //address = newint.getStringExtra(BTSingleton.EXTRA_ADDRESS); //receive the address of the bluetooth device

        //view dello stato di avvicinamento
        setContentView(R.layout.activity_approaching);
        new ConnectBT().execute(); //Call the class to connect
        mContext = getBaseContext();
        // ora arduino e android sono collegati mediante bluetooth

        //Log.d("ciao", "This is my message");

        //se arduino mi da l'ok creo l'acivity di login e la faccio partire.
        /*Intent i = new Intent(ApproachingActivity.this, LoginActivity.class);
        startActivity(i);*/

    }

    public static Context getContext() {
        return mContext;
    }


    // fast way to call Toast
    private void msg(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ApproachingActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            ConnectSuccess = BTSingleton.getLog().connect();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            } else {
                msg("Connected.");
            }
            progress.dismiss();
        }
    }


}
