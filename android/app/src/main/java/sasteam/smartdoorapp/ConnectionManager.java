package sasteam.smartdoorapp;



import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

public class ConnectionManager extends Thread {

    private BluetoothSocket btSocket;
    private InputStream btInStream;
    private OutputStream btOutStream;
    private boolean stop;
    private boolean tmp;
    private static ConnectionManager instance = null;
    private SmartDoor sm = new SmartDoor();
    private String[] parts;
    private String temp;
    private boolean wrongLogin = false;
    private boolean handShake = true;


    private ConnectionManager() {
        stop = true;
        tmp=false;
    }

    public static ConnectionManager getInstance() {
        if (instance == null)
            instance = new ConnectionManager();
        return instance;
    }

    public void setChannel(BluetoothSocket socket) {
        btSocket = socket;
        try {
            btInStream = socket.getInputStream();
            btOutStream = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        stop = false;
    }


    public void run() {
        byte[] buffer = new byte[1024];
        int nBytes = 0;
        while (!stop) {
            try {
                nBytes = btInStream.read(buffer);
                String readMessage = new String(buffer, 0, nBytes);
                // Send the obtained bytes to the UI Activity via handler
                Log.d("log--> ",readMessage);
                if(readMessage.indexOf('_')>=0){
                    parts = readMessage.split("_");
                    temp = parts[1];
                }
                switch (readMessage){
                    // posso effettuare il login
                    case "k":
                        if (handShake){
                            sm.login(btOutStream);
                            Intent i = new Intent(ApproachingActivity.getContext(), LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ApproachingActivity.getContext().startActivity(i);
                            handShake=false;
                        }
                        break;
                    // le credenziali sono corrette
                    case  "l" :
                        Intent i2 = new Intent(LoginActivity.getContext(), MonitoringActivity.class);
                        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        LoginActivity.getContext().startActivity(i2);
                        break;
                    // le credenziali non sono corrette
                    case "x":
                        this.wrongLogin = true;
                        break;
                    // su DOOR è stato premuto il pulsante
                    case "p":

                        if (BTSingleton.getLog().btSocket!=null)
                        {
                            try {
                                ConnectionManager.getInstance().getBtOutStream().write("KO".toString().getBytes());
                                Thread.sleep(500);
                                BTSingleton.getLog().btSocket.close(); //close connection
                                Intent i4 = new Intent(MonitoringActivity.getmContext(), PairActivity.class);
                                i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                MonitoringActivity.getmContext().startActivity(i4);
                                System.exit(0);
                                break;
                            } catch (Exception e) {
                                e.printStackTrace();
                                break;
                            }
                        }
                    // il pir su DOOR non rileva più
                    case "e":
                        if (BTSingleton.getLog().btSocket!=null)
                        {
                            try {
                                ConnectionManager.getInstance().getBtOutStream().write("KO".toString().getBytes());
                                Thread.sleep(500);
                                BTSingleton.getLog().btSocket.close(); //close connection
                                Intent i5 = new Intent(MonitoringActivity.getmContext(), PairActivity.class);
                                i5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                MonitoringActivity.getmContext().startActivity(i5);
                                System.exit(0);
                                break;
                            } catch (Exception e) {
                                e.printStackTrace();
                                break;
                            }
                        }
                }
            } catch (Exception e) {
                stop = true;
            }
        }
    }

    public String getTemp(){
        return temp;
    }

    public boolean getLoginStatus() {
        return this.wrongLogin;
    }

    public void setLoginStatus(boolean status) {
        this.wrongLogin = status;
    }

    public OutputStream getBtOutStream(){
        return btOutStream;
    }

    public InputStream getBtInputStream(){
        return btInStream;
    }


    public boolean write(byte[] bytes) {
        if (btOutStream == null) return false;
        try {
            btOutStream.write(bytes);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public void cancel() {
        try {
            btSocket.close();
        } catch (IOException e) { /* ... */ }
    }

    public void setHandShake (boolean x){
        handShake=x;
    }
}
